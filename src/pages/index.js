// src/index.js
import * as React from "react"
import { Link, graphql } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"
import Rating from "@mui/material/Rating"

import Layout from "../components/layout"
import Seo from "../components/seo"

const IndexPage = ({ data }) => (
  <Layout>
    <Seo title="Home" />
    <div>
      <StaticImage
        src="../images/example.png"
        loading="eager"
        width={64}
        quality={95}
        formats={["auto", "webp", "avif"]}
        alt=""
        style={{ marginBottom: `var(--space-3)` }}
      />
      <h1>
        Welcome to my <b>Blog!</b>
      </h1>
      <h2>Pick a Blog, any blog</h2>
      {data.allMarkdownRemark.nodes.map(({ frontmatter }) => (
        <>
          <Link className="primary button" to={frontmatter.slug}>
            <p>{frontmatter.title}</p>
          </Link>
          <Rating name="half-rating" defaultValue={4.5} precision={0.5} />
        </>
      ))}
    </div>
  </Layout>
)

/**
 * Head export to define metadata for the page
 *
 * See: https://www.gatsbyjs.com/docs/reference/built-in-components/gatsby-head/
 */
export const Head = () => <Seo title="Home" />

export default IndexPage

export const pageQuery = graphql`
  query BlogsQuery {
    allMarkdownRemark {
      nodes {
        frontmatter {
          slug
          title
          date
        }
      }
    }
  }
`
