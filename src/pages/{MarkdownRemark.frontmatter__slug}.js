// src/pages/{MarkdownRemark.frontmatter__slug}

import React from "react"
import { graphql } from "gatsby"
export default function Template({
  data, // this prop will be injected by the GraphQL query below.
}) {
  const [catFact, setCatFact] = React.useState();
  React.useEffect(() => {
    async function fetchCatFacts() {
      const result = await fetch('https://catfact.ninja/fact');
      const data = await result.json();
      setCatFact(data.fact);
    }
    fetchCatFacts()
  },[]);
  const { markdownRemark } = data // data.markdownRemark holds your post data
  const { frontmatter, html } = markdownRemark
  return (
    <div className="blog-post-container">
      <div className="blog-post">
        <h1>{frontmatter.title}</h1>
        <h2>{frontmatter.date}</h2>
        <p><b>Cat Fact:</b><i> {catFact}</i></p>
        <div
          className="blog-post-content"
          dangerouslySetInnerHTML={{ __html: html }}
        />
      </div>
    </div>
  )
}
export const pageQuery = graphql`
  query($id: String!) {
    markdownRemark(id: { eq: $id }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        slug
        title
      }
    }
  }
`